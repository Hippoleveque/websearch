Exploration du projet
=====================

Test général du projet
----------------------

Un jupyter notebook est mis à disposition afin de tester l'intégralité du code de ce projet.
Pour le lancer, il suffit de run:

  	$ jupyter notebook notebooks/project_testing.ipynb

Le notebook est lui-même documenté pour expliquer l'avancement à travers le projet.


Localisation du code
--------------------

Si l'évaluation souhaite voir le code source, celui-ci se trouve dans le sous-répertoire `websearch` :

	(lab) louis@Louis-MSI:~/Prod/OSY/websearch$ tree websearch/
	websearch/
	├── data
	│   ├── cacm.all
	│   ├── cacm.json
	│   ├── common_words
	│   ├── __init__.py
	│   ├── __pycache__
	│   │   └── __init__.cpython-37.pyc
	│   ├── qrels.text
	│   └── query.text
	├── index_bsbi_cacm_compressed.py   # Code pour index compressé
	├── index_bsbi_cacm.py		    # Code pour index CACM
	├── index_bsbi_cs.py		    # Code pour index CS
	├── __init__.py
	├── lib.py			    # Fonctions utilitaires
	├── search_model_cacm.py            # Modèles de recherche pour CACM
	├── search_model_cs.py              # Modèles de recherche pour CS
	├── semantic_cs.py                  # Réponses aux question 1 - 5 pour CS
	├── semantic.py                     # Réponses aux question 1 - 5 pour CACM
	└── variable_encoding.py
	

