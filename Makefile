clean:
	@rm -f */version.txt
	@rm -f .coverage
	@rm -fr */__pycache__
	@rm -fr __pycache__
	@rm -fr build
	@rm -fr dist
	@rm -fr websearch-*.dist-info
	@rm -fr websearch.egg-info

wheel: clean
	@python3 setup.py bdist_wheel

install: clean wheel
	@pip3 install -U dist/*.whl

check_code:
	@flake8 scripts/* websearch/*.py tests/*.py
