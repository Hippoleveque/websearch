from setuptools import find_packages
from setuptools import setup

# PyPI dependencies should be put here
requirements = """
    spacy
    wheel
    pip>=9
    setuptools>=26
    wheel>=0.29
    pandas
    yapf
    flake8
    coverage
    xlrd
    requests
    matplotlib
"""

setup(
    name='websearch',
    setup_requires=['setuptools_scm'],
    use_scm_version=True,
    description="Skeleton for data science & machine learning projects",
    packages=find_packages(),
    test_suite='tests',
    install_requires=requirements,
    # include_package_data: to install data from MANIFEST.in
    include_package_data=True,
    scripts=["scripts/ws_read_cacm"],
    zip_safe=False
)
