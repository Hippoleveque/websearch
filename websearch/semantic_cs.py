import matplotlib.pyplot as plt
import math
import string

from websearch.semantic import rank_and_frequency

def count_tokens_cs(data, half=False):
    list_of_ids = list(data.keys())
    if half:
        list_of_ids = [el for el in list_of_ids if el < len(data)//2 +1]
    field = "tokens"
    compteur = 0
    for doc_id in list_of_ids:
        try:
            compteur += len(data[doc_id][field])
        except KeyError:
            pass
    return compteur


def count_vocabulary_cs(data, half=False):
    list_of_ids = list(data.keys())
    if half:
        list_of_ids = [el for el in list_of_ids if el < len(data)//2 +1]
    field = "raw_doc"
    answer = []
    for doc_id in list_of_ids:
        try:
            words = data[doc_id][field].split()
            answer += words
        except:
            pass
    return len(set(answer))


def all_tokens_cs(data):
    field = "tokens"
    list_of_ids = list(data.keys())
    tokens = []
    for doc_id in list_of_ids:
        try:
            tokens += data[doc_id][field]
        except KeyError:
            pass
    return tokens


def main_cs(data):
    #Question 1
    t_count = count_tokens_cs(data)
    print("Nombre de tokens: {}".format(t_count))
    #Question 2
    v_count = count_vocabulary_cs(data)
    print("Taille du vocabulaire : {}".format(v_count))
    #Question 3
    t_half_count = count_tokens_cs(data,half=True)
    print("Nombre de tokens pour la moitié de la collection: {}"
    .format(t_half_count))
    v_half_count=count_vocabulary_cs(data, half=True)
    print("Taille du vocabulaire pour la moitié de la collection: {}"
    .format(v_half_count))
    #Question 4
    b = math.log(v_count/v_half_count)/math.log(t_count/t_half_count)
    k = v_half_count/(t_half_count**b)
    print("Le facteur k est : {}".format(round(k,2)))
    print("Le facteur b est :{}".format(round(b,2)))
    v_for_1M=k*(1000000**b)
    print("Taille du vocabulaire pour 1M de tokens : {}".format(round(v_for_1M)))
    #Question 5
    tokens = all_tokens_cs(data)
    punctuation = [letter for letter in string.punctuation]
    rank_frequency = rank_and_frequency(tokens)
    frequencies = [el[1] for el in rank_frequency]
    ranks = list(range(1,len(frequencies)+1))
    plt.figure("Ranks vs Frequencies")
    plt.plot(ranks,frequencies)
    plt.figure("Log(Ranks) vs Log(Frequencies)")
    plt.plot([math.log(el) for el in ranks],[math.log(el) for el in frequencies])
    plt.show()
