from websearch.semantic import read_and_process,all_tokens
from websearch.lib import read_common_words
from websearch.lib import duration
from datetime import datetime
import string
import sys
import random
import pickle
import os

def parse_doc(path,termID,doc_ids,last_doc_id):
    """
    read the doc located in path and tokenize it
    """
    forbidden = [el for el in string.punctuation] + ["'s","&"] +[el for el in string.digits]
    cw = read_common_words()
    doc = open(path,"r").read()
    doc_ids[last_doc_id] = {}
    doc_ids[last_doc_id]["raw_doc"] = doc
    doc_ids[last_doc_id]["title"] = path
    for el in forbidden:
        doc = doc.replace(el,'')
    doc = " ".join(doc.split())
    doc = doc.split()
    processed_words = [word.lower() for word in doc if word.lower() not in cw]
    doc_ids[last_doc_id]["tokens"] = processed_words
    answer = []
    last_term_id = len(termID)
    i=0
    for index,token in enumerate(processed_words):
        if token in termID.keys():
            pass
        else:
            termID[token] = last_term_id + i
            i+= 1
        answer.append((termID[token],last_doc_id,index))

    return answer,termID,doc_ids



def bsbi_index_construction(paths):
    """
    data : list of directories
    """
    termID={}
    docID={}
    block_indexes = []
    for el in paths:
        print('reading block')
        current_block = [el + path for path in os.listdir(el)]
        last_doc_id = len(docID)
        index_block,termID,newDOCID = invert_block(current_block,termID,last_doc_id)
        block_indexes += index_block
        docID.update(newDOCID)
    answer=big_merge(block_indexes)
    return answer,termID,docID





def block_merge(block_list):
    """
    block_list is a list of tuple (termID,docID,pos)
    returns list of lists (termID,[(docID1,[pos1,..]),(docID2,[pos1,..])])
    """
    answer = []
    i=0
    while i < len(block_list):
        answer.append((block_list[i][0],[(block_list[i][1],[block_list[i][2]])]))
        if i < len(block_list)-1:
            try:
                # termid = termid
                while block_list[i][0] == block_list[i+1][0]:
                    # docid = docid
                    while block_list[i][1] == block_list[i+1][1] and block_list[i][0] == block_list[i+1][0]:
                        #append position
                        answer[-1][1][-1][1].append(block_list[i+1][2])
                        i += 1
                    if block_list[i][0] == block_list[i+1][0]:
                        answer[-1][1].append((block_list[i+1][1],[block_list[i+1][2]]))
                        i += 1
            except:
                pass
        i+=1

    return answer


def invert_block(block,termID,last_doc_id):
    """
    Take a block and termID dict as arguments
    Process a block and returns list of lists (termID,[(docID1,[pos1,..]),(docID2,[pos1,..])])
    and updated termID dict
    """
    block_list=[]
    doc_ids = dict()
    for doc in block:
        triplet,termID,docID = parse_doc(doc,termID,doc_ids,last_doc_id)
        block_list += triplet
        doc_ids.update(docID)
        last_doc_id += 1
    block_list = sorted(block_list,key=lambda x:(x[0],x[1],x[2]))

    block_answer = block_merge(block_list)
    return block_answer,termID,doc_ids



def table_fusion(t1,t2):
    t1 = set_ordered_list(t1)
    t2 = set_ordered_list(t2)
    s3 = len(t1) + len(t2)
    i = 0
    j = 0
    t3 = []
    k=0
    while k < s3:
        if i == len(t1) :
            t3.append(t2[j])
            j+=1
        elif j == len(t2) - 1:
            t3.append(t1[i])
            i+= 1
        else:
            if t1[i]<t2[j]:
                t3.append(t1[i])
                i+=1
            elif t1[i]>t2[j]:
                t3.append(t2[j])
                j += 1
            else:
                j+=1
        k+=1
    return t3

def set_ordered_list(alist):
    new_list=[]
    for i in alist:
        if len(new_list)==0:
            new_list.append(i)
        elif i > new_list[-1]:
            new_list.append(i)
    return new_list


def big_merge(block_indexes):
    """
    Merge index extracted for every block into one single one
    """
    answer = {}
    for posting in block_indexes:
        if posting[0] not in answer.keys():
            answer[posting[0]] = [0,None]
            answer[posting[0]][1] = set_ordered_list(posting[1])
        else:
            answer[posting[0]][1] = table_fusion(answer[posting[0]][1],posting[1])
        answer[posting[0]][0] += sum(len(el[1]) for el in posting[1])
    return answer

if __name__ == "__main__":
    root_path = '/Users/hippolyteleveque/Documents/cs/ecp/cours_osy/websearch/pa1-data/'
    paths = os.listdir(root_path)
    data = [root_path + path + "/" for path in paths]
    test_path = "/Users/hippolyteleveque/Documents/cs/ecp/cours_osy/websearch/websearch/data/data2/"
    # answer,termID,doc_ids = parse_doc(test_path,{},{},0)
    #data = [test_path]
    answer = bsbi_index_construction(data)
    index = answer[0]
    termID = answer[1]
    docID = answer[2]
    pickle_index = open('index_cs.pickle',"wb")
    pickle_termID = open('termID_cs.pickle',"wb")
    pickle_docID = open('docID_cs.pickle',"wb")
    pickle.dump(index,pickle_index)
    pickle.dump(termID,pickle_termID)
    pickle.dump(docID,pickle_docID)
