
import math

def variable_encoding(integer):
    """
    return the compressed form of the integer
    """
    if integer == 0:
        return bytes([0 + 128])
    max_index = math.floor(math.log(integer,128))
    encoding = bytes()
    while integer > 128:
        factor = integer // (128**max_index)
        encoding += bytes([factor])
        integer -= factor * (128**max_index)
        max_index -= 1
    last_factor = integer % 128
    encoding+= bytes([last_factor+128])
    return encoding
