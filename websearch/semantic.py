# -*- coding: UTF-8 -*-
import pickle
import progressbar
import spacy
from websearch.lib import read_common_words
from websearch.lib import read_json_file
from websearch.data import DATA_SOURCE
import string
import matplotlib.pyplot as plt
import math

def tokenizer(my_dict, model):
    """
    fields to tokenize : resume, title, keywords
    """
    forbidden = [letter for letter in string.punctuation] + ["'s","&"] + [el for el in string.digits]
    cw = read_common_words()
    fields = ["resume", "title"]
    for field in fields:
        try:
            pre_processed_field = my_dict[field]
            for el in forbidden:
                pre_processed_field = pre_processed_field.replace(el,' ')
            pre_processed_field = " ".join(pre_processed_field.split())
            doc = model(pre_processed_field)
            new_field = "{}_token".format(field)
            my_dict[new_field] = [token for token in doc if token.text.lower().rstrip() not in cw]
        except KeyError:
            pass
    # Special treatment for keywords
    field = "keywords"
    doc_list = []
    try:
        pre_processed_l = " ".join([token for sublist in my_dict[field] for token in sublist])
        for el in forbidden:
            pre_processed_l = pre_processed_l.replace(el,' ')
        pre_processed_l = " ".join(pre_processed_l.split())
        doc = model(pre_processed_l)
        new_field = "{}_token".format(field)
        my_dict[new_field] = [token for token in doc if token.text.lower().rstrip() not in cw]
    except KeyError:
        pass
    return my_dict


def compare_stop_words(my_dict):
    cw = read_common_words()
    fields = ["resume_token", "title_token"]
    for field in fields:
        try:
            tokens = my_dict[field]
            new_tokens = [token for token in tokens
                          if token.string.strip().lower() not in cw]
            my_dict[field] = new_tokens
        except KeyError:
            pass
    # Special treatment for keywords
    field = "keywords_token"
    new_keywords_token = []
    try:
        for el in my_dict[field]:
            new_el = [token for token in el
                      if token.string.strip().lower() not in cw]
            new_keywords_token.append(new_el)
        my_dict[field] = new_keywords_token
    except:
        pass
    return my_dict


def count_tokens(data,half=False):
    if half:
        data = [doc for doc in data if int(doc["id"]) < len(data)//2 + 1]
    fields = ["resume_token", "title_token"]
    compteur = 0
    for doc in data:
        for field in fields:
            try:
                compteur += len(doc[field])
            except KeyError:
                pass
        # Special treatment for keywords
        try:
            field = "keywords_token"
            flat_keywords = [item for sublist in doc[field] for item in sublist]
            compteur += len(flat_keywords)
        except:
            pass
    return compteur

def count_vocabulary(path="cacm.json",half=False):
    """
    count vocabulary size
    """
    path = DATA_SOURCE + path
    data = read_json_file(path)
    data = [doc for doc in data]
    if half:
        data = [doc for doc in data if int(doc["id"]) < len(data)//2 + 1 ]
    fields = ["resume", "title"]
    answer = []
    for doc in data:
        for field in fields:
            try:
                words = doc[field].split()
                answer += words
            except:
                pass
        field = "keywords"
        try:
            words = [word for sublist in doc[field] for word in sublist].join(" ")
            words = words.split()
            answer += words
        except:
            pass
    return len(set(answer))


def token_to_string(my_dict):
    """
    format spacy tokens back to strings
    """
    for field in ["resume_token", "title_token","keywords_token"]:
        try:
            new_list = [token.text.lower().rstrip() for token in my_dict[field] if token.text.lower().rstrip() != ""]
            my_dict[field] = new_list
        except:
            pass
    return my_dict


def read_and_process(path="cacm.json",half=False):
    print("* Reading data")
    path = DATA_SOURCE + path
    print(path)
    data = read_json_file(path)
    data = [doc for doc in data]
    if half:
        data = [doc for doc in data if int(doc["id"]) < len(data)//2 + 1]
    print("* Loading Spacy model...", end=" ", flush=True)
    model = spacy.load("en")
    print("DONE")
    #bar = progressbar.ProgressBar()
    print("* Tokenization")
    for my_dict in data:
        my_dict = tokenizer(my_dict, model)
    # print("* Removing stop words")
    # for my_dict in data:
    #     my_dict = compare_stop_words(my_dict)
    print("* Formatting back all tokens as string and removing spaces")
    for my_dict in data:
        my_dict = token_to_string(my_dict)

    return data

def all_tokens(data):
    """
    returns all tokens
    """
    fields = ["resume_token", "title_token"]
    tokens = []
    for doc in data:
        for field in fields:
            try:
                tokens += doc[field]
            except KeyError:
                pass
        # Special treatment for keywords
        try:
            field = "keywords_token"
            flat_keywords = [item for sublist in doc[field] for item in sublist]
            tokens += flat_keywords
        except:
            pass
    return tokens

def rank_and_frequency(tokens):
    answer = dict()
    punctuation_signs = [letter for letter in string.punctuation]
    for token in tokens:
        if token in answer.keys():
            answer[token] += 1
        elif token not in punctuation_signs:
            answer[token] = 1
        else:
            continue
    frequency = [(token,answer[token]) for token in answer.keys()]
    frequency = sorted(frequency,key=lambda x:x[1],reverse=True)
    return frequency


def main(data):
    #Question 1
    t_count = count_tokens(data)
    print("Nombre de tokens: {}".format(t_count))
    #Question 2
    v_count = count_vocabulary()
    print("Taille du vocabulaire : {}".format(v_count))
    #Question 3
    t_half_count = count_tokens(data,half=True)
    print("Nombre de tokens pour la moitié de la collection: {}"
    .format(t_half_count))
    v_half_count=count_vocabulary(half=True)
    print("Taille du vocabulaire pour la moitié de la collection: {}"
    .format(v_half_count))
    #Question 4
    b = math.log(v_count/v_half_count)/math.log(t_count/t_half_count)
    k = v_half_count/(t_half_count**b)
    print("Le facteur k est : {}".format(round(k,2)))
    print("Le facteur b est :{}".format(round(b,2)))
    v_for_1M=k*(1000000**b)
    print("Taille du vocabulaire pour 1M de tokens : {}".format(round(v_for_1M)))
    #Question 5
    tokens = all_tokens(data)
    punctuation = [letter for letter in string.punctuation]
    rank_frequency = rank_and_frequency(tokens)
    frequencies = [el[1] for el in rank_frequency]
    ranks = list(range(1,len(frequencies)+1))
    plt.figure("Ranks vs Frequencies")
    plt.plot(ranks,frequencies)
    plt.figure("Log(Ranks) vs Log(Frequencies)")
    plt.plot([math.log(el) for el in ranks],[math.log(el) for el in frequencies])
    plt.show()
    # On obtient un résultat cohérent avec la loi de Zipf


if __name__ == "__main__":
    data = read_and_process()
    pickle_data = open("processed_data2.pickle","wb")
    pickle.dump(data,pickle_data)
