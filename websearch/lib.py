# -*- coding: UTF-8 -*-
""" Main lib for websearch Project
"""
import json
import sys
from datetime import datetime
from websearch.data import DATA_SOURCE

def hello_world():
    print("hello world")


def duration(start, now=None, linehead=''):
    """
    Prettify duration in days, hours, mins and sec
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 11, 3, 1, 33, 151236)
    assert duration(start, end) == '1 hours 15 min 2 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 12, 3, 1, 33, 151236)
    assert duration(start, end) == '1 days 1 hours 15 min 2 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 12, 23, 46, 33, 151236)
    assert duration(start, end) == '1 days 22 hours 0 min 2 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 11, 1, 55, 13, 151236)
    assert duration(start, end) == '8 min 42 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 11, 1, 47, 17, 151236)
    assert duration(start, end) == '0 min 46.0 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 11, 1, 47, 17, 131036)
    """
    if not now:
        now = datetime.now()
    delta = now - start
    days, seconds = delta.days, delta.total_seconds()
    hours = int(seconds // 3600 - 24 * days)
    minutes = int((seconds % 3600) // 60)

    seconds = round(seconds % 60, 2)

    if (days, hours, minutes) != (0, 0, 0):
        seconds = round(seconds, 2)
    dateformat = '{} min {} sec'.format(minutes, seconds)
    if hours:
        dateformat = '{} hours '.format(hours) + dateformat
    if days:
        dateformat = '{} days '.format(days) + dateformat
    return linehead + dateformat


def good_nb_format(number):
    return '{:,}'.format(number).replace(',', ' ')


def get_size_of(my_object):
    size = sys.getsizeof(my_object)
    good_size = good_nb_format(size)
    print("Size : {} bytes".format(good_size))


def read_cacm(path="cacm.all"):
    path = DATA_SOURCE + path
    data = []
    with open(path, 'r') as f:
        raw_data = f.readlines()
    new_doc = {}
    i = 0
    while i < len(raw_data):
        line = raw_data[i]
        if line[:2] == ".I":
            data.append(new_doc)
            new_doc = {}
            new_doc["id"] = line[2:-1].replace(' ', '')
        elif line[:2] == ".T":
            new_doc["title"] = raw_data[i+1].replace('\n', '')
            i += 1
        elif line[:2] == ".W":
            j = i + 1
            resume = []
            while raw_data[j][0] != '.':
                resume.append(raw_data[j].replace('\n', ''))
                j += 1
            resume = ''.join(resume)
            new_doc["resume"] = resume
            i = j
        elif line[:2] == ".K":
            j = i + 1
            keywords = []
            while raw_data[j][0] != '.':
                keywords.append(raw_data[j].replace('\n', '').split(','))
                j += 1
            new_doc["keywords"] = keywords
            i = j
        i += 1
    new_file = DATA_SOURCE + "cacm.json"
    with open(new_file, "w") as f:
        json.dump(data[1:], f)
    print('==> "new file created at : {}'.format(new_file))


def read_json_file(abs_path):
    with open(abs_path, 'r') as f:
        data = json.load(f)
    return data


def read_common_words(path="common_words"):
    path = DATA_SOURCE + path
    with open(path, "r") as f:
        all_lines = f.readlines()
    cw = []
    for line in all_lines:
        cw.append(line.replace("\n", ""))
    return cw

if __name__ == "__main__":
    read_cacm()
