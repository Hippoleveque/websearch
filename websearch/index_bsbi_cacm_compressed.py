from websearch.semantic import read_and_process,all_tokens
from websearch.lib import duration
from websearch.variable_encoding import variable_encoding
from datetime import datetime
import string
import sys
import random
import pickle
import copy


def create_doc_dict(data):
    data_copied = copy.deepcopy(data)
    doc_dict = {}
    for doc in data_copied:
        doc_dict[int(doc.pop('id'))] = doc
    return doc_dict


def index_compression(index):
    term_ids = index.keys()
    compressed_index = {}
    for key in term_ids:
        term_id = key
        freq = index[key][0]
        postings = []
        for posting in index[key][1]:
            doc_id = posting[0]
            positions = []
            for pos in posting[1]:
                positions.append(variable_encoding(pos))
            postings.append((variable_encoding(doc_id),positions))
        compressed_index[variable_encoding(term_id)] = (variable_encoding(doc_id),postings)
    return compressed_index



def bsbi_index_construction(data,block_size):
    start = datetime.now()
    termID={}
    block_indexes = []
    while len(data)>0:
        print('reading block')
        current_block,index = get_block(data,block_size)
        print('updating data')
        data=data[index:]
        print('inverting block')
        index_block,newID = invert_block(current_block, termID)
        block_indexes += index_block
        termID.update(newID)
    answer=big_fat_merge(block_indexes)
    answer = index_compression(answer)
    print("\nDuration : ", duration(start))
    return answer,termID

def get_block(data,block_size):
    """
    Returns a list of documents which
    size is just under block_size
    """
    block = []
    i=0
    while sys.getsizeof(block) <= block_size and i < len(data):
        block.append(data[i])
        i+=1
    return block,i


def parse_doc(doc,termID):
    """
    returns list of tuple (termID, docID, pos)
    for the bloc
    """
    fields = ["title_token", "resume_token","keywords_token"]
    tokens = []
    for field in fields:
        try:
            tokens += doc[field]
        except KeyError:
            pass
    # Special treatment for keywords
    # try:
    #     field = "keywords_token"
    #     flat_keywords = [item for sublist in
    #     doc[field] for item in sublist]
    #     tokens += flat_keywords
    # except:
    #     pass
    #doc_voc = set([token.text for token in tokens])
    doc_voc = set([token for token in tokens])
    # Update the corresponding dictionnary terms and termID
    for term in doc_voc:
        if term in termID.keys():
            continue
        else:
            termID[term] = len(termID)+1
    answer=[]
    for index,token in enumerate(tokens):
        #answer.append((termID[token.text],int(doc["id"])))
        answer.append((termID[token],int(doc["id"]),index))
    return answer,termID





def block_merge(block_list):
    """
    block_list is a list of tuple (termID,docID,pos)
    returns list of lists (termID,[(docID1,[pos1,..]),(docID2,[pos1,..])])
    """
    answer = []
    i=0
    while i < len(block_list):
        answer.append((block_list[i][0],[(block_list[i][1],[block_list[i][2]])]))
        if i < len(block_list)-1:
            try:
                while block_list[i][0] == block_list[i+1][0]:
                    while block_list[i][1] == block_list[i+1][1] and block_list[i][0] == block_list[i+1][0]:
                        answer[-1][1][-1][1].append(block_list[i+1][2])
                        i += 1
                    if block_list[i][0] == block_list[i+1][0]:
                        answer[-1][1].append((block_list[i+1][1],[block_list[i+1][2]]))
                        i += 1
            except:
                pass
        i+=1
    return answer




def invert_block(block,termID):
    block_list=[]
    term_ids = dict()
    for doc in block:
        triplet,id = parse_doc(doc,termID)
        block_list += triplet
        term_ids.update(id)

    block_list = sorted(block_list,key=lambda x:(x[0],x[1],x[2]))
    block_answer = block_merge(block_list)
    return block_answer,termID


# def merge(block_list):
#     answer = {}
#     for termid,docid in block_list:
#         if termid not in answer.keys():
#             answer[termid]=[0,[]]
#         answer[termid][0] += 1
#         if docid in answer[termid][1]:
#             continue
#         else:
#             answer[termid][1].append(docid)
#     return answer

def table_fusion(t1,t2):
    t1 = set_ordered_list(t1)
    t2 = set_ordered_list(t2)
    s3 = len(t1) + len(t2)
    i = 0
    j = 0
    t3 = []
    k=0
    while k < s3:
        if i == len(t1) :
            t3.append(t2[j])
            j+=1
        elif j == len(t2) - 1:
            t3.append(t1[i])
            i+= 1
        else:
            if t1[i]<t2[j]:
                t3.append(t1[i])
                i+=1
            elif t1[i]>t2[j]:
                t3.append(t2[j])
                j += 1
            else:
                j+=1
        k+=1
    return t3

def set_ordered_list(alist):
    new_list=[]
    for i in alist:
        if len(new_list)==0:
            new_list.append(i)
        elif i > new_list[-1]:
            new_list.append(i)
    return new_list


def big_fat_merge(block_indexes):
    answer = {}
    for posting in block_indexes:
        if posting[0] not in answer.keys():
            answer[posting[0]] = [0,None]
            answer[posting[0]][1] = set_ordered_list(posting[1])
        else:
            answer[posting[0]][1] = table_fusion(answer[posting[0]][1],posting[1])
        answer[posting[0]][0] += sum(len(el[1]) for el in posting[1])
    return answer

if __name__ == "__main__":
    with open('../processed_data.pickle', 'rb') as f:
        data = pickle.load(f)
        index = bsbi_index_construction(data, 10000)[0]
