from websearch.search_model_cacm import main_vectorial_cacm
from websearch.search_model_cacm import main_vectorial_cacm_normalized_frequency
from websearch.search_model_cacm import main_vectorial_cacm_without_prints

import matplotlib.pyplot as plt
import numpy as np

def read_query_file(path):
    data = []
    with open(path, 'r') as f:
        raw_data = f.readlines()
    i = 0
    new_doc = {}
    while i < len(raw_data):
        line = raw_data[i]
        if line[:2] == '.I':
            data.append(new_doc)
            doc_id = line[2:-1].replace(' ', '')
            if len(doc_id) == 1:
                doc_id = '0{}'.format(doc_id)
            new_doc = {}
            new_doc["id"] = doc_id
        elif line[:2] == '.W':
            j = i+1
            new_doc['request'] = ''
            while raw_data[j][0] != '.':
                new_doc['request'] += raw_data[j].replace('\n', ' ')
                j += 1
            i = j
        elif line[:2] == '.A':
            j = i+1
            new_doc['authors'] = []
            while raw_data[j][0] != '.':
                new_doc['authors'].append(raw_data[j])
                j += 1
            i = j
        elif line[:2] == '.N':
            new_doc['title'] = raw_data[i+1]
        i += 1
    return data[1:]


def update_queries_with_qrels(data, qrels_path):
    with open(qrels_path, 'r') as f:
        raw_data = f.readlines()
    for doc in data:
        doc_id = doc['id']
        for line in raw_data:
            if line[:2] == doc_id:
                line = line.split(' ')
                try:
                    doc['qrel'].append(int(line[1]))
                except KeyError:
                    doc['qrel'] = [int(line[1])]
    return data


def compute_prediction(data, index, term_dict, doc_dict, raw_data):
    for doc in data:
        n = 10 
        query = doc['request']
        doc['predicted_tf_idf'] = main_vectorial_cacm(query, index, term_dict, doc_dict, n, raw_data)
        doc['predicted_norm_freq'] = main_vectorial_cacm_normalized_frequency(query, index, term_dict, doc_dict, n, raw_data)
    return data

def compute_measure(doc):
    try:
        # Rappel
        nb_docs_pert_tf_idf = len([el for el in doc['predicted_tf_idf'] if el in doc['qrel']])
        doc['rappel_tf_idf'] = nb_docs_pert_tf_idf/len(doc['qrel'])
        nb_docs_pert_norm_freq = len([el for el in doc['predicted_norm_freq'] if el in doc['qrel']])
        doc['rappel_norm_freq'] = nb_docs_pert_norm_freq/len(doc['qrel'])
        # Precision
        doc['precision_tf_idf'] = nb_docs_pert_tf_idf/len(doc['predicted_tf_idf'])
        doc['precision_norm_freq'] = nb_docs_pert_norm_freq/len(doc['predicted_norm_freq'])
    except KeyError:
        pass
    return doc

def print_rappel_precision(data):
    # tf_idf
    rappels_tf_idf = [doc['rappel_tf_idf'] for doc in data if 'rappel_tf_idf' in list(doc.keys())]
    precisions_tf_idf = [doc['precision_tf_idf'] for doc in data if 'precision_tf_idf' in list(doc.keys())]
    plt.figure("Precision vs Rappel : TF-IDF")
    plt.plot(rappels_tf_idf, precisions_tf_idf, 'go')
    # normalized_freq
    rappels_norm_freq = [doc['rappel_norm_freq'] for doc in data if 'rappel_norm_freq' in list(doc.keys())]
    precisions_norm_freq = [doc['precision_norm_freq'] for doc in data if 'precision_norm_freq' in list(doc.keys())]
    plt.figure("Precision vs Rappel : NORMALIZED FREQ")
    plt.plot(rappels_norm_freq, precisions_norm_freq, 'go')
    
    plt.show()


def create_rappel_preci_lists(doc, index, term_dict, doc_dict, raw_data):
    query = doc['request']
    l = len(doc['qrel'])
    rappels = []
    precis = []
    k = 1
    rappel = 0
    while rappel < 1 and k <= 100:
        res = main_vectorial_cacm_without_prints(query, index, term_dict, doc_dict, k, raw_data)
        docs_pertinents = len([el for el in res if el in doc['qrel']])
        rappel = docs_pertinents/l
        precision = docs_pertinents/len(res)
        if rappel != 0 and rappel not in rappels:
            rappels.append(rappel)
            precis.append(precision)
        print('k={}: rappel={} & preci={}'.format(k, rappel, precision))
        k += 1
    return rappels, precis


def compute_e_measure(data, alpha):
    all_e_measures = []
    for doc in data:
        r = doc['rappel_tf_idf']
        p = doc['precision_tf_idf']
        e = 1 - (1 / (alpha*(1/p) + (1-alpha)*(1/r)))
        all_e_measures.append(e)
    return np.mean(all_e_measures)


def compute_f_measure(e_measure):
    return 1 - e_measure


if __name__ == "__main__":
    path = "websearch/data/query.text"
    data = read_query_file(path)
    data = update_queries_with_qrels(data, "websearch/data/qrels.text")
