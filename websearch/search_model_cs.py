from datetime import datetime
from websearch.lib import duration
from websearch.lib import read_common_words
from websearch.search_model_cacm import generate_sat_request
import spacy
import math
import string


def print_documents_cs(doc_id_list, doc_dict):
    """
    Input is a (ordered or not) list of doc_id and we want to pretty print thoe doc content
    """
    print('\n')
    print('=' * 60)
    main_title = '  RESULTS  '
    print(main_title.center(60, "="))
    print('=' * 60)
    print('\n')

    fields = ["title", "raw_doc"]
    if doc_id_list:
        for doc_id in doc_id_list:
            document = doc_dict[doc_id]
            print('-' * 60)
            print('\n')
            for field in fields:
                try:
                    print('* {} : {}'.format(field, document[field]))
                    print("\n")
                except KeyError:
                    pass
            print('-' * 60)
    else:
        msg = 'No document matching your request'
        print(msg.center(60, ' '))


def boolean_search_cs(request, index, term_dict, doc_dict):
    """
    input should be a list of list of tuples of size 2.
    Each sublist of the main list is a clause that needs to be satisfied,
        meaning that we have a "AND" between those sublists.
    On the contrary, all tuples of a sublist are linked with "OR",
        meaning that at least one of thoses tuples needs to be satisfied.
    Tuples are of size 2 : tuple[0] is a termID, and tuple[1] can be:
        0 for negation
        1 for positivity
    """
    all_doc_ids = list(doc_dict.keys())
    all_results = []
    for clause in request:
        interm_result = []
        for var in clause:
            term_id = term_dict[var[0]]
            if var[1]:
                matching_doc_ids = [el[0] for el in index[term_id][1]]
            else:
                unmatching_doc_ids = [el[0] for el in index[term_id][1]]
                matching_doc_ids = [el for el in all_doc_ids if el not in unmatching_doc_ids]
            interm_result.append(matching_doc_ids)
            union_result = []
            for doc_list in interm_result:
                for el in doc_list:
                    if el not in union_result:
                        union_result.append(el)
                    else:
                        pass
        all_results.append(set(union_result))
    return set.intersection(*all_results)


def main_boolean_cs(search, index, term_dict, doc_dict):
    """
    input of this function should be user friendly
    """
    start = datetime.now()
    print('Generating SAT request ...', end=' ', flush=True)
    sat_request = generate_sat_request(search)
    print('SUCCESS :')
    print('     SAT request --> {}'.format(sat_request))

    # From search to SAT request here (to be implemented)

    res = boolean_search_cs(sat_request, index, term_dict, doc_dict)
    print(res)
    print_documents_cs(res, doc_dict)
    print("\nDuration : ", duration(start))


def main_vectorial_cs(search, index, term_dict, doc_dict, number_of_wanted_docs):
    """
    Search all documents indexed for a query specific query (search)
    """
    start = datetime.now()
    N = len(doc_dict)
    res = vectorial_search(search, N, index, term_dict, doc_dict, number_of_wanted_docs)
    print('res: ', res)
    ordered_docs = [el[0] for el in res[::-1]]
    print('ordered list: ', ordered_docs)
    print_documents_cs(ordered_docs, doc_dict)
    print("\nDuration : ", duration(start))


def process_query(query):
    """
    input : query as set of words
    outputs : processed query
    """
    model = spacy.load('en')
    query = query.split()
    cw = read_common_words()
    for i in range(len(query)):
        query[i] = model(query[i])
    processed_query = [token for token in query
              if token.text.lower().rstrip() not in cw]
    processed_query = [token.text.lower().rstrip() for token in processed_query]
    return processed_query


def main_vectorial_cs_normalized_frequency(search, index,term_dict,doc_dict,number_of_wanted_docs):
    """
    Search all documents indexed for a query specific query (search)
    """
    start = datetime.now()
    N = len(doc_dict)
    res = vectorial_search_normalized_frequency_cs(search, N, index, term_dict, number_of_wanted_docs,doc_dict)
    print('res: ', res)
    ordered_docs = [el[0] for el in res[::-1]]
    print('ordered list: ', ordered_docs)
    print_documents_cs(ordered_docs, doc_dict)
    print("\nDuration : ", duration(start))






def vectorial_search_normalized_frequency_cs(query,N,index,term_ids,k,docID):
    start = datetime.now()
    query = process_query(query)
    n = 0
    K = len(query)
    all_docs = []
    s = {}
    for i in range(K):
        try:
            term_id = term_ids[query[i]]
        except:
            continue
        w1 = 1
        n += w1 ** 2
        postings = index[term_id][1]
        for doc in postings:
            doc_id = doc[0]
            all_docs.append(doc_id)
            if doc_id in s.keys():
                pass
            else:
                s[doc_id] = 0
            tf = len(doc[1])
            all_tokens = docID[doc_id]["tokens"]
            maxi = max(all_tokens,key=lambda x:all_tokens.count(x))
            max_tf = all_tokens.count(maxi)
            w2 = tf / max_tf
            normalized_term = 0
            for token in all_tokens:
                token_doc_count = all_tokens.count(token)
                token_weight =  token_doc_count / max_tf
                normalized_term += token_weight**2
            normalized_term = 1/(normalized_term**(1/2))
            s[doc_id] += w1*w2*normalized_term

    for doc in s.keys():
        s[doc] = s[doc]/(n**(1/2))
    top = [(None,0) for i in range(k)]
    for doc in s.keys():
        i = 0
        if s[doc]>top[0][1]:
            i = 1
            top[0] = (doc,s[doc])
            while i<k and s[doc]>top[i][1]:
                top[i],top[i-1] = top[i-1],top[i]
                i += 1
        else:
            continue
    print("\nDuration : ", duration(start))
    return top




def vectorial_search(query,N,index,term_ids,docID,k):
    """
    input : query, number of documents, inversed index,termID dict, number of
    wanted docs k
    output : list of tuple (docID,score) in ASC order, tf-idf weights
    """
    query = process_query(query)
    n = 0
    K = len(query)
    all_docs = []
    s = {}
    for i in range(K):
        try:
            term_id = term_ids[query[i]]
        except:
            continue
        term_count = len(index[term_id][1])
        w1 = 1
        n += w1**2
        postings = index[term_id][1]
        for doc in postings:
            doc_id = doc[0]
            all_docs.append(doc_id)
            if doc_id in s.keys():
                pass
            else:
                s[doc_id] = 0
            term_doc_count = len(doc[1])
            w2 = (1 + math.log(term_doc_count,10))*math.log(N/term_count,10)
            normalized_term = 0
            all_tokens = docID[doc_id]["tokens"]
            for token in all_tokens:
                token_id = term_ids[token]
                token_total_count = len(index[token_id][1])
                token_doc_count = all_tokens.count(token)
                token_weight =(1 + math.log(token_doc_count,10))*math.log(N/token_total_count,10)
                normalized_term += token_weight**2
            normalized_term = 1/(normalized_term**(1/2))
            s[doc_id] += w1*w2*normalized_term
    for doc in s.keys():
        s[doc] = s[doc]/(n**(1/2))
    top = [(None,0) for i in range(k)]
    for doc in s.keys():
        i = 0
        if s[doc]>top[0][1]:
            i = 1
            top[0] = (doc,s[doc])
            while i<k and s[doc]>top[i][1]:
                top[i],top[i-1] = top[i-1],top[i]
                i += 1
        else:
            continue
    return top
